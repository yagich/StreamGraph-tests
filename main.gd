# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends Node

const DEFAULT_RENDERER := preload("res://graph_node_renderer/deck_holder_renderer.tscn")

var deck_holder_renderer: DeckHolderRenderer = null
var deck_holder_renderer_finished := false

@onready var rpc_renderer := $RPCRenderer as RPCRenderer


func _ready() -> void:
	get_tree().auto_accept_quit = false
	
	if DisplayServer.get_name() != "headless":
		# not headless, start default renderer
		deck_holder_renderer = DEFAULT_RENDERER.instantiate()
		add_child(deck_holder_renderer)
		deck_holder_renderer.quit_completed.connect(_on_deck_holder_renderer_quit_completed)
		deck_holder_renderer.rpc_start_requested.connect(_on_deck_holder_renderer_rpc_start_requested)
		deck_holder_renderer.rpc_stop_requested.connect(_on_deck_holder_renderer_rpc_stop_requested)


func _on_deck_holder_renderer_quit_completed() -> void:
	# will be used later to make sure both default and rpc are finished processing
	deck_holder_renderer_finished = true
	
	get_tree().quit()


func _on_deck_holder_renderer_rpc_start_requested(port: int) -> void:
	rpc_renderer.listen(port)


func _on_deck_holder_renderer_rpc_stop_requested() -> void:
	rpc_renderer.stop()


func _notification(what: int) -> void:
	if what == NOTIFICATION_WM_CLOSE_REQUEST:
		if deck_holder_renderer:
			deck_holder_renderer.request_quit()
