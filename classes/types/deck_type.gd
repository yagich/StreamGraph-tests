# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
class_name DeckType

enum Types{
	BOOL,
	NUMERIC,
	STRING,
	ARRAY,
	DICTIONARY,
	ANY,
}

const CONVERSION_MAP := {
	Types.BOOL: [Types.NUMERIC, Types.STRING, Types.ANY],
	Types.NUMERIC: [Types.BOOL, Types.STRING, Types.ANY],
	Types.STRING: [Types.BOOL, Types.NUMERIC, Types.ANY],
	Types.ARRAY: [Types.STRING, Types.BOOL, Types.ANY],
	Types.DICTIONARY: [Types.STRING, Types.BOOL, Types.ANY],
	Types.ANY: [Types.BOOL, Types.NUMERIC, Types.STRING, Types.ARRAY, Types.DICTIONARY]
}

const GODOT_TYPES_MAP := {
	Types.BOOL: TYPE_BOOL,
	Types.NUMERIC: TYPE_FLOAT,
	Types.STRING: TYPE_STRING,
	Types.ARRAY: TYPE_ARRAY,
	Types.DICTIONARY: TYPE_DICTIONARY,
}

const INVERSE_GODOT_TYPES_MAP := {
	TYPE_BOOL: Types.BOOL,
	TYPE_FLOAT: Types.NUMERIC,
	TYPE_INT: Types.NUMERIC,
	TYPE_STRING: Types.STRING,
	TYPE_ARRAY: Types.ARRAY,
	TYPE_DICTIONARY: Types.DICTIONARY,
}


static func can_convert(from: Types, to: Types) -> bool:
	if from == to:
		return true

	return (to in (CONVERSION_MAP[from] as Array))


static func convert_value(value: Variant, to: Types) -> Variant:
	if to == Types.ANY:
		return value

	return type_convert(value, GODOT_TYPES_MAP[to])


static func type_str(type: Types) -> String:
	return str(Types.keys()[type])

## Validates whether the given Dictionary is a "Vector", AKA that it has an X and a Y key.
static func is_valid_vector(dict : Variant):
	
	if !dict is Dictionary:
		
		return false
	
	return dict.has("x") and dict.has("y")
	
