# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
class_name Logger

enum LogType {
	INFO,
	WARN,
	ERROR,
}

enum LogCategory {
	NODE,
	DECK,
	SYSTEM,
	RENDERER,
}

signal log_message(text: String, type: LogType, category: LogCategory)


func log_node(text: Variant, type: LogType = LogType.INFO) -> void:
	self.log(str(text), type, LogCategory.NODE)


func log_deck(text: Variant, type: LogType = LogType.INFO) -> void:
	self.log(str(text), type, LogCategory.DECK)


func log_system(text: Variant, type: LogType = LogType.INFO) -> void:
	self.log(str(text), type, LogCategory.SYSTEM)


func log_renderer(text: Variant, type: LogType = LogType.INFO) -> void:
	self.log(str(text), type, LogCategory.RENDERER)


func log(text: String, type: LogType, category: LogCategory) -> void:
	log_message.emit(text, type, category)
	
	if OS.has_feature("editor"):
		prints(LogType.keys()[type].capitalize(), LogCategory.keys()[category].capitalize(), text)
