# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
class_name Port
## A data type representing a port of a [DeckNode].
##
## Ports are used for connections between [DeckNode]s and can contain data that is passed between
## them on a node.

enum UsageType {
	TRIGGER, ## Port can send or receive events, not request values.
	VALUE_REQUEST, ## Port can request values and respond to value requests, not send or receive events.
	BOTH, ## Port can send or receive events [b]and[/b] request and respond to value requests.
}

## The type index of this port.
var type: DeckType.Types
## The label of this port. Used by the renderer to display. How it's displayed depends on the renderer
## and the [member descriptor].
var label: String
## Hints to the renderer on how to display this port.[br]
## Can be either one of these: [code]button textfield spinbox slider textblock codeblock checkbox singlechoice multichoice[/code].[br]
## Additional descriptor properties can be specified after the type, delimited by a colon ([code]:[/code]).[br]
## @experimental
var descriptor: String
## A callback to get this port's value. Intended usage is by renderers that show an input field of some kind.
var value_callback: Callable

## The type of this port (input, output or virtual)
var port_type: DeckNode.PortType
## The usage type of this port (see [enum UsageType]).
var usage_type: UsageType
## The local index of this port.
var index_of_type: int
## The global index of this port.
var index: int

## The value of this port.
var value: Variant

signal value_updated(new_value: Variant)
signal button_pressed()


func _init(
		p_type: DeckType.Types,
		p_label: String,
		p_index: int,
		p_port_type: DeckNode.PortType,
		p_index_of_type: int,
		p_descriptor: String = "",
#		p_value_callback: Callable = Callable(),
		p_usage_type: UsageType = UsageType.BOTH,
		) -> void:
	type = p_type
	label = p_label
	descriptor = p_descriptor
#	value_callback = p_value_callback

	port_type = p_port_type
	index_of_type = p_index_of_type
	index = p_index
	usage_type = p_usage_type


func set_value(v: Variant) -> void:
	set_value_no_signal(v)
	value_updated.emit(value)


func set_value_no_signal(v: Variant) -> void:
	if v is Callable:
		value = v.call()
		return

	value = v
