# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode


func _init() -> void:
	name = "Add Vectors"
	node_type = "vector_add"
	description = "Adds two 2D vectors."

	add_input_port(
		DeckType.Types.DICTIONARY,
		"Vector A"
	)
	add_input_port(
		DeckType.Types.DICTIONARY,
		"Vector B"
	)
	add_output_port(
		DeckType.Types.DICTIONARY,
		"Result"
	)

## Handles Trigger inputs, calculates.
func _receive(port : int, data : Variant) -> void:
	
	var va = data
	if !DeckType.is_valid_vector(data):
		
		DeckHolder.logger.log_node("Vector Add: Port %d: Supplied data was not a valid Vector.  Please ensure it is a Dictionary with keys 'x' and 'y'" % port)
		return
	
	var vb
	if port == 0:
		
		vb = await request_value_async(1)
		
	else:
		
		vb = await request_value_async(0)
	
	var added = add_vectors(va, vb)
	
	if added is Dictionary:
		
		send(0, added)
		
	

func _value_request(_on_port: int) -> Variant:
	var va = await request_value_async(0)
	var vb = await request_value_async(1)
	
	return add_vectors(va, vb)
	

func add_vectors(va, vb):
	
	if not va or not vb:
		DeckHolder.logger.log_node("Vector Add: one of the vectors is invalid.", Logger.LogType.ERROR)
		return null

	if !DeckType.is_valid_vector(va):
		DeckHolder.logger.log_node("Vector Add: one of the vectors is invalid.", Logger.LogType.ERROR)
		return null

	if !DeckType.is_valid_vector(vb):
		DeckHolder.logger.log_node("Vector Add: one of the vectors is invalid.", Logger.LogType.ERROR)
		return null

	var res := {}
	res["x"] = va["x"] + vb["x"]
	res["y"] = va["y"] + vb["y"]
	
	return res
	
