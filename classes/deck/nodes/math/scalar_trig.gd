# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode


func _init() -> void:
	name = "Trigonometry"
	node_type = "scalar_trig"
	description = "A selection of trigonometric functions on numbers."

	aliases = [
		"sine", "cosine", "tangent",
		"arcsin", "acos", "arccos", "arctan", "atan", 
		"sinh", "cosh", "tanh",
		"asinh", "acosh", "atanh",
	]

	add_virtual_port(
		DeckType.Types.STRING,
		"Function",
		"singlechoice:sin:cos:tan:asin:acos:atan:asinh:acosh:atanh",
	)
	
	add_input_port(
		DeckType.Types.NUMERIC,
		"Angle",
		"spinbox:unbounded:0.0001",
	)
	
	add_output_port(
		DeckType.Types.NUMERIC,
		"Result",
	)


func _do(v: float) -> float:
	match get_virtual_ports()[0].value:
		"cos":
			return cos(v)
		"tan":
			return tan(v)
		"asin":
			return asin(v)
		"acos":
			return acos(v)
		"atan":
			return atan(v)
		"asinh":
			return asinh(v)
		"acosh":
			return acosh(v)
		"atanh":
			return atanh(v)
		_:
			return sin(v)


func _value_request(_on_output_port: int) -> float:
	var v = await resolve_input_port_value_async(0)
	v = DeckType.convert_value(v, DeckType.Types.NUMERIC)
	
	return _do(v)


func _receive(on_input_port: int, data: Variant) -> void:
	var v =  DeckType.convert_value(data, DeckType.Types.NUMERIC)
	
	send(0, _do(v))
