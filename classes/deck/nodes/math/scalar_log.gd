# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode


func _init() -> void:
	name = "Natural Logarithm"
	node_type = "scalar_log"
	description = "Returns the natural logarithm of the number (base e)."

	add_input_port(
		DeckType.Types.NUMERIC,
		"Number",
		"spinbox:unbounded:0.0001",
	)
	
	add_output_port(
		DeckType.Types.NUMERIC,
		"Result",
	)


func _value_request(_on_output_port: int) -> float:
	var v = await resolve_input_port_value_async(0)
	v = DeckType.convert_value(v, DeckType.Types.NUMERIC)
	
	return log(v)


func _receive(_to_input_port: int, data: Variant) -> void:
	var v = DeckType.convert_value(data, DeckType.Types.NUMERIC)
	
	send(0, log(v))
