# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode


func _init() -> void:
	name = "Vector Dot Product"
	node_type = "vector_dot"
	description = "Returns the dot product of two vectors."

	add_input_port(
		DeckType.Types.DICTIONARY,
		"Vector A"
	)
	add_input_port(
		DeckType.Types.DICTIONARY,
		"Vector B"
	)

	add_output_port(
		DeckType.Types.NUMERIC,
		"Dot"
	)


func _receive(port : int, data : Variant) -> void:
	
	if !DeckType.is_valid_vector(data):
		
		DeckHolder.logger.log_node("Vector Dot: Port %d: Supplied data was not a valid Vector.  Please ensure it is a Dictionary with keys 'x' and 'y'" % port)
		return
	
	var va = data
	var vb
	
	if port == 0:
		
		vb = await resolve_input_port_value_async(1)
		
	else:
		
		vb = await resolve_input_port_value_async(0)
		
	
	var dot = dot_vectors(va, vb)
	if dot:
		
		send(0, dot)
		
	

func _value_request(_on_port: int) -> Variant:
	var va = await request_value_async(0)
	var vb = await request_value_async(1)
	
	return dot_vectors(va, vb)
	

func dot_vectors(va, vb):
	
	if not va or not vb:
		DeckHolder.logger.log_node("Vector Dot: one of the vectors is invalid.", Logger.LogType.ERROR)
		return null
	
	if !DeckType.is_valid_vector(va):
		DeckHolder.logger.log_node("Vector Dot: one of the vectors is invalid.", Logger.LogType.ERROR)
		return null
	
	if !DeckType.is_valid_vector(vb):
		DeckHolder.logger.log_node("Vector Dot: one of the vectors is invalid.", Logger.LogType.ERROR)
		return null
	
	return va.x * vb.x + va.y * vb.y
	
