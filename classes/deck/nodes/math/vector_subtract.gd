# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode


func _init() -> void:
	name = "Subtract Vectors"
	node_type = "vector_subtract"
	description = "Subtracts each component of the given vectors."

	add_input_port(
		DeckType.Types.DICTIONARY,
		"Vector A"
	)
	add_input_port(
		DeckType.Types.DICTIONARY,
		"Vector B"
	)
	add_output_port(
		DeckType.Types.DICTIONARY,
		"Result"
	)


func _receive(port : int, data : Variant) -> void:
	
	var va
	var vb
	
	if port == 0:
		va = data
		
		vb = await request_value_async(1)
		
	else:
		
		va = await request_value_async(0)
		
		vb = data
	
	if (not DeckType.is_valid_vector(va)) or (not DeckType.is_valid_vector(vb)):
		DeckHolder.logger.log_node("Vector Sub: Port %d: one of the inputs is not a valid Vector. Please ensure it is a Dictionary with keys 'x' and 'y'." % port)
		return
	
	
	var calc = subtract_vectors(va, vb)
	
	if calc is Dictionary:
		
		send(0, calc)
		
	

func _value_request(_on_port: int) -> Variant:
	var va = await request_value_async(0)
	var vb = await request_value_async(1)
	
	return subtract_vectors(va, vb)

func subtract_vectors(va, vb):
	
	if not va or not vb:
		DeckHolder.logger.log_node("Vector Sub: one of the vectors is invalid.", Logger.LogType.ERROR)
		return null
	
	if !DeckType.is_valid_vector(va):
		DeckHolder.logger.log_node("Vector Sub: one of the vectors is invalid.", Logger.LogType.ERROR)
		return null
	
	if !DeckType.is_valid_vector(vb):
		DeckHolder.logger.log_node("Vector Sub: one of the vectors is invalid.", Logger.LogType.ERROR)
		return null
	
	var res := {}
	res["x"] = va["x"] - vb["x"]
	res["y"] = va["y"] - vb["y"]
	
	return res
	
