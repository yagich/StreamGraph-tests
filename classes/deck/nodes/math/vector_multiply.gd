# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode


func _init() -> void:
	name = "Multiply Vector by Scalar"
	node_type = "vector_multiply"
	description = "Multiplies a vector by a numeric value."

	add_input_port(
		DeckType.Types.DICTIONARY,
		"Vector"
	)
	add_input_port(
		DeckType.Types.NUMERIC,
		"Scalar",
		"field"
	)

	add_output_port(
		DeckType.Types.DICTIONARY,
		"Result"
	)


func _receive(port : int, data : Variant) -> void:
	
	var vec
	var scale
	
	if port == 0:
		
		vec = data
		scale = float(await resolve_input_port_value_async(1))
		
	else:
		
		scale = data
		vec = await request_value_async(0)
		
	
	var multiplied = multiply_by_scalar(vec, scale)
	if multiplied is Dictionary:
		
		send(0, multiplied)
		
	

func _value_request(_on_port: int) -> Variant:
	
	var vec = await request_value_async(0)
	var scale = float(await resolve_input_port_value_async(1))
	
	return multiply_by_scalar(vec, scale)
	


func multiply_by_scalar(vec, scale):
	
	if not vec:
		DeckHolder.logger.log_node("Vector Mult: the vector is invalid.", Logger.LogType.ERROR)
		return null
	
	if !DeckType.is_valid_vector(vec):
		DeckHolder.logger.log_node("Vector Mult: the vector is invalid.", Logger.LogType.ERROR)
		return null
	
	return {"x": vec.x * scale, "y": vec.y * scale}
	

