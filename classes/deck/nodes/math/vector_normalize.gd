# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode


func _init() -> void:
	name = "Normalize Vector"
	node_type = "vector_normalize"
	description = "Normalizes a vector so its' length (magnitude) is exactly 1."

	add_input_port(
		DeckType.Types.DICTIONARY,
		"Vector"
	)

	add_output_port(
		DeckType.Types.DICTIONARY,
		"Result"
	)


func _receive(port : int, data : Variant) -> void:
	
	var normalized = normalize_vector(data)
	if !normalized is Dictionary:
		return
	
	send(0, normalized)
	

func _value_request(_on_port: int) -> Variant:
	
	var v = await request_value_async(0)
	
	return normalize_vector(v)
	

func normalize_vector(v):
	
	if not v:
		DeckHolder.logger.log_node("Vector Normalize: the vector is invalid.", Logger.LogType.ERROR)
		return null

	if !DeckType.is_valid_vector(v):
		DeckHolder.logger.log_node("Vector Normalize: the vector is invalid.", Logger.LogType.ERROR)
		return null

	var l: float = (v.x ** 2.0) + (v.y ** 2.0)
	var res := {"x": v.x, "y": v.y}
	if l != 0:
		l = sqrt(l)
		res.x = res.x / l
		res.y = res.y / l
		return res

	DeckHolder.logger.log_node("Vector Normalize: the vector is length 0. Returning null.", Logger.LogType.ERROR)

	return null
