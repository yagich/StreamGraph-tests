# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode


func _init() -> void:
	name = "Round Number"
	node_type = "scalar_round"
	description = "Rounds a number to the nearest whole number."

	add_virtual_port(
		DeckType.Types.STRING,
		"Function",
		"singlechoice:round:floor:ceil"
	)
	
	add_input_port(
		DeckType.Types.NUMERIC,
		"Number",
		"spinbox:unbounded:0.0001"
	)

	add_output_port(
		DeckType.Types.NUMERIC,
		"Result"
	)


func _round(v: float) -> float:
	match get_virtual_ports()[0].value:
		"floor":
			return floorf(v)
		"ceil":
			return ceilf(v)
		_:
			return roundf(v)


func _value_request(on_output_port: int) -> Variant:
	var v = await resolve_input_port_value_async(0)
	v = DeckType.convert_value(v, DeckType.Types.NUMERIC)
	
	return _round(v)


func _receive(_to_input_port: int, data: Variant) -> void:
	var v = DeckType.convert_value(data, DeckType.Types.NUMERIC)
	send(0, _round(v))
