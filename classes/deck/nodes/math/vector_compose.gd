# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode


func _init() -> void:
	name = "Compose Vector"
	node_type = "vector_compose"
	description = "Returns a vector from two numeric inputs."

	add_input_port(
		DeckType.Types.NUMERIC,
		"X",
		"field"
	)
	add_input_port(
		DeckType.Types.NUMERIC,
		"Y",
		"field"
	)

	add_output_port(
		DeckType.Types.DICTIONARY,
		"Vector"
	)

func _receive(port : int, data : Variant) -> void:
	
	var x
	var y
	if port == 0:
		
		x = data
		y = await resolve_input_port_value_async(1)
		
	else:
		
		y = data
		x = await resolve_input_port_value_async(0)
		
	
	send(0, {"x" : float(x), "y" : float(y)})
	

func _value_request(_on_port: int) -> Dictionary:
	var x = float(await resolve_input_port_value_async(0))
	var y = float(await resolve_input_port_value_async(1))
	return {"x": x, "y": y}
