# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode

var _cached: Dictionary

func _init() -> void:
	name = "Decompose OBS Transform"
	node_type = "obs_decompose_transform"
	description = "Splits an OBS transform from one object into multiple outputs."

	add_input_port(
		DeckType.Types.DICTIONARY,
		"Transform"
	)

	add_output_port(
		DeckType.Types.NUMERIC,
		"Rotation"
	)

	add_output_port(
		DeckType.Types.NUMERIC,
		"Position X"
	)
	add_output_port(
		DeckType.Types.NUMERIC,
		"Position Y"
	)


func _value_request(on_port: int) -> Variant:
	var t
	if not _cached.is_empty():
		t = _cached
	else:
		t = await resolve_input_port_value_async(0)
	
	if t == null:
		return null

	match on_port:
		0:
			return (t as Dictionary).get("rotation")
		1:
			return (t as Dictionary).get("position_x")
		2:
			return (t as Dictionary).get("position_y")
		_:
			return null


func _receive(_on_input_port: int, data: Variant) -> void:
	if not data is Dictionary:
		return
	
	_cached = data
	var id := UUID.v4()
	send(0, data.get("rotation"), id)
	send(1, data.get("position_x"), id)
	send(2, data.get("position_y"), id)
	_cached = {}
