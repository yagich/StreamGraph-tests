# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode

var noobs: NoOBSWS

func _init() -> void:
	name = "Set Scene"
	node_type = "obs_set_scene"
	description = "Sets the current scene in OBS."

	props_to_serialize = []

	add_input_port(
		DeckType.Types.STRING,
		"Scene Name",
		"field"
	)

	add_input_port(
		DeckType.Types.ANY,
		"Switch",
		"button",
		Port.UsageType.TRIGGER,
	).button_pressed.connect(_receive.bind(1, null))


func _receive(on_input_port: int, data: Variant) -> void:
	if noobs == null:
		noobs = Connections.obs_websocket

	var scene_name: String
	if on_input_port == 1:
		scene_name = await resolve_input_port_value_async(0, "")
	else:
		scene_name = data as String

	if scene_name.is_empty():
		return

	noobs.make_generic_request(
		"SetCurrentProgramScene",
		{"scene_name": scene_name}
	)
