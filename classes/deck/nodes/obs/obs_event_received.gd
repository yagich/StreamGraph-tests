# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode

var cached_data: Dictionary


func _init() -> void:
	name = "OBS Event Received"
	node_type = name.to_snake_case()
	description = ""
	
	add_input_port(
		DeckType.Types.STRING,
		"Event Name",
		"field",
		Port.UsageType.VALUE_REQUEST,
	)
	
	add_output_port(
		DeckType.Types.ANY,
		"Event Received",
		"",
		Port.UsageType.TRIGGER,
	)
	
	add_output_port(
		DeckType.Types.DICTIONARY,
		"Event Data",
	)


func _event_received(event_name: StringName, event_data: Dictionary = {}) -> void:
	if event_name != &"obs_event":
		return
	
	if event_data.get("event_type", "") != await resolve_input_port_value_async(0):
		return
	
	cached_data = event_data.event_data
	var id := UUID.v4()
	send(0, event_data.event_data, id)
	send(1, event_data.event_data, id)


func _value_request(__on_output_port: int) -> Variant:
	return cached_data
