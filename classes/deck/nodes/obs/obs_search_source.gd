# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode

var noobs: NoOBSWS

var cached_scene_name: String
var cached_source_name: String
var cached_id # TODO: evaluate if this caching is actually needed


func _init() -> void:
	name = "Get Source ID"
	node_type = "obs_search_source"
	description = "Searches for an OBS source in a scene and returns its output."

	add_input_port(
		DeckType.Types.STRING,
		"Scene name",
		"field"
	)

	add_input_port(
		DeckType.Types.STRING,
		"Source name",
		"field"
	)

	add_output_port(
		DeckType.Types.NUMERIC,
		"Source ID"
	)


func _get_source_id(scene_name: String, source_name: String) -> Variant:
	if noobs == null:
		noobs = Connections.obs_websocket

	if cached_scene_name == scene_name and cached_source_name == scene_name:
		return cached_id
	
	cached_scene_name = scene_name
	cached_source_name = source_name

	var req := noobs.make_generic_request(
		"GetSceneItemId",
		{
			"scene_name": scene_name,
			"source_name": source_name
		}
	)
	await req.response_received

	var data := req.message.get_data()
	#if int(data.request_status.code) != NoOBSWS.Enums.RequestStatus.NO_ERROR:
		#return null
	cached_id = data.response_data.scene_item_id

	return data.response_data.scene_item_id


func _value_request(_on_output_port: int) -> Variant:
	var scene_name_req = await resolve_input_port_value_async(0, "")
	if scene_name_req == null:
		return null
	
	var source_name_req = await resolve_input_port_value_async(1, "")
	if source_name_req == null:
		return null
	
	return await _get_source_id(scene_name_req, source_name_req)


func _receive(on_input_port: int, data: Variant) -> void:
	if on_input_port == 0:
		if not data is String:
			return
		
		var source_name_req = await resolve_input_port_value_async(1, "")
		if source_name_req == null:
			return
		
		send(0, await _get_source_id(data, source_name_req))
	else:
		if not data is String:
			return
		
		var scene_name_req = await resolve_input_port_value_async(0, "")
		if scene_name_req == null:
			return

		send(0, await _get_source_id(scene_name_req, data))
