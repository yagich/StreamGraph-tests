# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode

var noobs: NoOBSWS

enum InputPorts{
	SCENE_NAME,
	SOURCE_ID,
	VISIBLE,
	SET,
}


func _init() -> void:
	name = "OBS Set Source Visible"
	node_type = name.to_snake_case()
	description = "Set a source's visibility."
	
	add_input_port(
		DeckType.Types.STRING,
		"Scene Name",
		"field",
	)
	
	add_input_port(
		DeckType.Types.NUMERIC,
		"Source ID",
		"spinbox:unbounded",
	)
	
	add_input_port(
		DeckType.Types.BOOL,
		"Visible",
		"checkbox",
	)
	
	add_input_port(
		DeckType.Types.ANY,
		"Set",
		"button",
		Port.UsageType.TRIGGER,
	).button_pressed.connect(_receive.bind(InputPorts.SET, null))
	
	get_input_ports()[2].set_value(true)


func _receive(to_input_port: int, data: Variant) -> void:
	if noobs == null:
		noobs = Connections.obs_websocket

	var scene_name
	var source_id
	var visible
	
	match to_input_port:
		InputPorts.SCENE_NAME:
			scene_name = str(data)
			source_id = await resolve_input_port_value_async(InputPorts.SOURCE_ID)
			visible = await resolve_input_port_value_async(InputPorts.VISIBLE)
		InputPorts.SOURCE_ID:
			source_id = data
			scene_name = await resolve_input_port_value_async(InputPorts.SCENE_NAME)
			visible = await resolve_input_port_value_async(InputPorts.VISIBLE)
		InputPorts.VISIBLE:
			scene_name = await resolve_input_port_value_async(InputPorts.SCENE_NAME)
			source_id = await resolve_input_port_value_async(InputPorts.SOURCE_ID)
			visible = bool(data)
		InputPorts.SET:
			scene_name = await resolve_input_port_value_async(InputPorts.SCENE_NAME)
			source_id = await resolve_input_port_value_async(InputPorts.SOURCE_ID)
			visible = await resolve_input_port_value_async(InputPorts.VISIBLE)
	
	var b := noobs.make_batch_request(false, NoOBSWS.Enums.RequestBatchExecutionType.SERIAL_FRAME)
	b.add_request(
		"SetSceneItemEnabled",
		"",
		{
			"scene_name": scene_name,
			"scene_item_id": source_id,
			"scene_item_enabled": visible,
		}
	)
	b.add_request("Sleep", "", {"sleep_frames": 1})
	b.send()
