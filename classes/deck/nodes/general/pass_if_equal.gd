# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode


func _init() -> void:
	name = "Pass If Equal"
	node_type = name.to_snake_case()
	description = "Receives a trigger and passes it if the values are equal."
	
	add_input_port(
		DeckType.Types.ANY,
		"Input",
		"",
		Port.UsageType.TRIGGER,
	)
	
	add_input_port(
		DeckType.Types.ANY,
		"Value A",
		"field",
		Port.UsageType.VALUE_REQUEST,
	)
	
	add_input_port(
		DeckType.Types.ANY,
		"Value B",
		"field",
		Port.UsageType.VALUE_REQUEST,
	)
	
	add_output_port(
		DeckType.Types.ANY,
		"Equal",
		"",
		Port.UsageType.TRIGGER
	)
	
	add_output_port(
		DeckType.Types.ANY,
		"Not Equal",
		"",
		Port.UsageType.TRIGGER
	)


func _receive(_to_input_port: int, data: Variant) -> void:
	var value_a = await resolve_input_port_value_async(1)
	var value_b = await resolve_input_port_value_async(2)
	
	if value_a == value_b:
		send(0, data)
	else:
		send(1, data)
