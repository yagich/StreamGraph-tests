# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode


func _init() -> void:
	name = "Numeric Constant"
	node_type = name.to_snake_case()

	add_output_port(
		DeckType.Types.NUMERIC,
		"Value",
		"spinbox:unbounded:0.0001",
		Port.UsageType.VALUE_REQUEST,
	)


func _value_request(_from_port: int) -> Variant:
	return ports[0].value
