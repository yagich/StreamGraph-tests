# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode


func _init():
	name = "Delay"
	node_type = name.to_snake_case()
	description = "A node that passes through its' input after the set time."

	add_input_port(
		DeckType.Types.NUMERIC,
		"Delay Time",
		"spinbox:unbounded:0.01",
		Port.UsageType.VALUE_REQUEST,
	)

	add_input_port(
		DeckType.Types.ANY,
		"Value",
		"",
		Port.UsageType.TRIGGER,
	)

	add_output_port(
		DeckType.Types.ANY,
		"Value",
		"",
		Port.UsageType.TRIGGER,
	)


func _receive(_to_input_port : int, data: Variant) -> void:
	var time: float = await resolve_input_port_value_async(0)
	await Engine.get_main_loop().create_timer(time).timeout
	send(0, data)
