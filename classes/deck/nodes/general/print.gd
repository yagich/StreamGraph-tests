# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode


func _init() -> void:
	name = "Print"
	node_type = name.to_snake_case()
	description = "Print a value to the console."

	add_input_port(
		DeckType.Types.ANY,
		"Data to print",
		"field"
	)

	add_input_port(
		DeckType.Types.ANY,
		"Trigger",
		"button",
		Port.UsageType.TRIGGER,
	).button_pressed.connect(_receive.bind(1, null))


func _receive(to_input_port: int, data: Variant) -> void:
	var data_to_print := ""
	if to_input_port == 1:
		data = await resolve_input_port_value_async(0)

	if data == null:
		data_to_print = "<nothing>"
	else:
		data_to_print = str(data)
	
	if data_to_print.is_empty():
		data_to_print = "<nothing>"

	DeckHolder.logger.log_node(data_to_print)
