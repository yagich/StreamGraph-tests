# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode


func _init() -> void:
	name = "Types Test"
	node_type = name.to_snake_case()
	description = ""

	props_to_serialize = []

	add_input_port(DeckType.Types.BOOL, "BOOL")
	add_input_port(DeckType.Types.NUMERIC, "NUMERIC")
	add_input_port(DeckType.Types.STRING, "STRING")
	add_input_port(DeckType.Types.ARRAY, "ARRAY")
	add_input_port(DeckType.Types.DICTIONARY, "DICTIONARY")
	add_input_port(DeckType.Types.ANY, "ANY")

	add_output_port(DeckType.Types.BOOL, "BOOL")
	add_output_port(DeckType.Types.NUMERIC, "NUMERIC")
	add_output_port(DeckType.Types.STRING, "STRING")
	add_output_port(DeckType.Types.ARRAY, "ARRAY")
	add_output_port(DeckType.Types.DICTIONARY, "DICTIONARY")
	add_output_port(DeckType.Types.ANY, "ANY")

	add_input_port(DeckType.Types.ANY, "Send", "button").button_pressed.connect(do)


func do() -> void:
	send(0, false)
	send(1, 1.0)
	send(2, "string")
	send(3, ["foo", "bar", 5.3])
	send(4, {"foo": "bar", 1: 2})
	send(5, null)

func _receive(to_input_port: int, data: Variant) -> void:
	print("received data %s of type %s on port %s, expected type %s, converted v %s, converted type %s" % [
		data, typeof(data), to_input_port, DeckType.GODOT_TYPES_MAP[get_input_ports()[to_input_port].type],
		DeckType.convert_value(data, get_input_ports()[to_input_port].type),
		typeof(DeckType.convert_value(data, get_input_ports()[to_input_port].type))
	])
