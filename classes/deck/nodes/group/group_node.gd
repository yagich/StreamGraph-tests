# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode

var group_id: String
var group_instance_id: String
var input_node: DeckNode
var output_node: DeckNode

var input_node_id: String
var output_node_id: String

var extra_ports: Array


func _init() -> void:
	name = "Group"
	node_type = "group_node"
	props_to_serialize = [&"group_id", &"group_instance_id", &"extra_ports", &"input_node_id", &"output_node_id"]
	appears_in_search = false


func _pre_connection() -> void:
	for port_type: PortType in extra_ports:
		match port_type:
			PortType.OUTPUT:
				add_output_port(DeckType.Types.ANY, "Output %s" % get_output_ports().size())
			PortType.INPUT:
				add_input_port(DeckType.Types.ANY, "Input %s" % get_input_ports().size())


func init_io() -> void:
	#var group: Deck = _belonging_to.groups.get(group_id) as Deck
	var group := DeckHolder.get_group_instance(group_id, group_instance_id)
	if not group:
		return

	if input_node and input_node.ports_updated.is_connected(recalculate_ports):
		input_node.ports_updated.disconnect(recalculate_ports)
	if output_node and output_node.ports_updated.is_connected(recalculate_ports):
		output_node.ports_updated.disconnect(recalculate_ports)

	input_node = group.get_node(group.group_input_node)
	output_node = group.get_node(group.group_output_node)

	recalculate_ports()
	setup_connections()


func setup_connections() -> void:
	input_node.ports_updated.connect(recalculate_ports)
	output_node.ports_updated.connect(recalculate_ports)


func recalculate_ports() -> void:
	ports.clear()

	for output_port: Port in output_node.get_input_ports():
		add_output_port(
			DeckType.Types.ANY,
			"Output %s" % output_port.index
		)

	for input_port: Port in input_node.get_output_ports():
		add_input_port(
			DeckType.Types.ANY,
			"Input %s" % input_port.index
		)

	extra_ports.clear()
	for port in ports:
		extra_ports.append(port.port_type)


func _receive(to_input_port: int, data: Variant):
	input_node.send(get_input_ports()[to_input_port].index_of_type, data)


func _value_request(from_port: int) -> Variant:
	return await output_node.request_value_async(from_port)
