# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode

var output_count: int:
	get:
		return get_all_ports().size() - 1

var group_node: DeckNode


func _init() -> void:
	name = "Group input"
	node_type = "group_input"
	props_to_serialize = [&"output_count"]
	appears_in_search = false
	user_can_delete = false

	add_output_port(
		DeckType.Types.ANY,
		"Input 0"
	)
	outgoing_connection_added.connect(_on_outgoing_connection_added)
	outgoing_connection_removed.connect(_on_outgoing_connection_removed)


func _on_outgoing_connection_added(port_idx: int) -> void:
	if port_idx + 1 < get_all_ports().size():
		return

	add_output_port(
		DeckType.Types.ANY,
		"Input %s" % (get_all_ports().size())
	)


func _on_outgoing_connection_removed(port_idx: int) -> void:
	var last_connected_port := 0
	#for port: int in outgoing_connections.keys().slice(1):
		#if !(outgoing_connections[port] as Array).is_empty():
			#last_connected_port = port
	for port: int in outgoing_connections.keys().slice(1):
		if not (outgoing_connections.get(port, {}) as Dictionary).is_empty():
			last_connected_port = port

	#prints("l:", last_connected_port, "p:", port_idx)

	if port_idx < last_connected_port:
		return

	var s := get_all_ports().slice(0, last_connected_port + 2)
	ports.assign(s)
	ports_updated.emit()


func _pre_connection() -> void:
	for i in output_count + 1:
		add_output_port(
			DeckType.Types.ANY,
			"Input %s" % (i + 1)
		)


func _post_load() -> void:
	# ensure we have enough ports after connections
	var last_connected_port := 0
	for port: int in outgoing_connections:
		last_connected_port = port if outgoing_connections.has(port) else last_connected_port

	if ports.size() <= last_connected_port:
		for i in last_connected_port:
			add_output_port(
				DeckType.Types.ANY,
				"Input %s" % get_output_ports().size()
			)


func _value_request(from_port: int) -> Variant:
	return await group_node.request_value_async(group_node.get_input_ports()[from_port].index_of_type)
