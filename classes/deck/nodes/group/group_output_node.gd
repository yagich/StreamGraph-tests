# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode

var input_count: int:
	get:
		return get_all_ports().size() - 1

var group_node: DeckNode


func _init() -> void:
	name = "Group output"
	node_type = "group_output"
	props_to_serialize = [&"input_count"]
	appears_in_search = false
	user_can_delete = false

	add_input_port(
		DeckType.Types.ANY,
		"Output 0"
	)

	incoming_connection_added.connect(_on_incoming_connection_added)
	incoming_connection_removed.connect(_on_incoming_connection_removed)


func _on_incoming_connection_added(port_idx: int) -> void:
	if port_idx + 1 < get_all_ports().size():
		return

	add_input_port(
		DeckType.Types.ANY,
		"Output %s" % (get_all_ports().size())
	)


func _on_incoming_connection_removed(port_idx: int) -> void:
	var last_connected_port := 0
	for port: int in incoming_connections.keys().slice(1):
		if not (incoming_connections[port] as Dictionary).is_empty():
			last_connected_port = port

	#prints("l:", last_connected_port, "p:", port_idx)

	if port_idx < last_connected_port:
		return

	var s := get_all_ports().slice(0, last_connected_port + 2)
	ports.assign(s)
	ports_updated.emit()


func _pre_connection() -> void:
	for i in input_count + 1:
		add_input_port(
			DeckType.Types.ANY,
			"Output %s" % (i + 1)
		)


func _post_load() -> void:
	# ensure we have enough ports after connections
	var last_connected_port := 0
	for port: int in incoming_connections:
		last_connected_port = port if incoming_connections.has(port) else last_connected_port

	if ports.size() <= last_connected_port:
		for i in last_connected_port:
			add_input_port(
				DeckType.Types.ANY,
				"Output %s" % get_input_ports().size()
			)


func _receive(to_input_port: int, data: Variant) -> void:
	group_node.send(group_node.get_output_ports()[to_input_port].index_of_type, data)
