# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode


func _init():
	name = "Twitch Remove EventSub Subscription"
	node_type = name.to_snake_case()
	description = "Removes an EventSub Subscription by it's type."
	appears_in_search = false
	
	props_to_serialize = []
	
	add_input_port(
		DeckType.Types.STRING, 
		"Subscription Type", 
		"field"
	)
	
	add_input_port(
		DeckType.Types.ANY, 
		"Remove Subscription", 
		"button"
	).button_pressed.connect(_receive.bind(1, null))


func _receive(to_input_port, data: Variant):
	
	var sub_type 
	if to_input_port == 0:
		
		sub_type = str(data)
		
	else:
		
		sub_type = await resolve_input_port_value_async(0)
		
	
	Connections.twitch.remove_eventsub_subscription_type(sub_type).response_completed.connect(eventsub_response_received)
	

func eventsub_response_received(_info):
	
	pass # TODO: Add Error Handling Later
	
