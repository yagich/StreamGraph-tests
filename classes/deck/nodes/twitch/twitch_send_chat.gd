# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode

enum inputs {
	
	message,
	channel,
	send
	
}

func _init():
	name = "Twitch Send Chat"
	node_type = "twitch_send_chat"
	description = "Sends a message to a Twitch chat."
	
	add_input_port(
		DeckType.Types.STRING, 
		"Message",
		"field"
	)
	add_input_port(
		DeckType.Types.STRING,
		"Channel",
		"field"
	)
	add_input_port(
		DeckType.Types.ANY,
		"Send",
		"button",
		Port.UsageType.TRIGGER,
	).button_pressed.connect(_receive.bind(inputs.send, null))


func _receive(to_input_port: int, data: Variant) -> void:
	
	data = str(data)
	
	var message
	var channel
	match to_input_port:
		
		inputs.message:
			
			message = data
			
			channel = await resolve_input_port_value_async(inputs.channel)
			
		inputs.channel:
			
			channel = data
			
			message = await resolve_input_port_value_async(inputs.message)
			
		inputs.send:
			
			channel = await resolve_input_port_value_async(inputs.channel)
			message = await resolve_input_port_value_async(inputs.message)
			
		
	
	if channel == null:
		
		channel = ""
	
	if message == null:
		
		return
	
	if message.is_empty():
		
		DeckHolder.logger.log_node("Twitch Send Chat: Port %d: Supplied message was empty." % [to_input_port])
		
	
	Connections.twitch.send_chat(message, channel)
	

