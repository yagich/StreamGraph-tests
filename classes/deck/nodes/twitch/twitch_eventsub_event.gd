# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode

var cached_event_data : Dictionary

func _init():
	name = "Twitch EventSub Event"
	node_type = name.to_snake_case()
	description = "Listens for a specific Event from Twitch EventSub."
	
	props_to_serialize = []
	
#	Adds a port that allows specifying what type of event to listen for.
	add_input_port(DeckType.Types.STRING, "Event Name", "field", Port.UsageType.VALUE_REQUEST)
#	Adds a port that outputs when the Event has been received
	add_output_port(DeckType.Types.ANY, "Event Received", "", Port.UsageType.TRIGGER)
#	Adds a port that outputs the data received when the Event has been received.
	add_output_port(DeckType.Types.DICTIONARY, "Event Data", "", Port.UsageType.BOTH)
	


func _event_received(event_name: StringName, event_data: Dictionary = {}):

	if event_name != &"twitch_eventsub":

		return


	var port_0 = await resolve_input_port_value_async(0)
	print("Event Name ", event_data)
	if port_0 == null or port_0 != event_data.payload.subscription.type:

		return

#	Sends the data along as well as the fact that the event happened.  While also caching the event data for later access
	cached_event_data = event_data.payload

#	Sends to indicate that the specified event has happened.
	send(0, event_data)
	send(1, event_data)
	


func _value_request(port):
	
	if port != 1:
	
		return
	
	
	return cached_event_data

