# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode


func _init():
	name = "Twitch Leave Chat"
	node_type = name.to_snake_case()
	description = "Leaves a Twitch channel."

	props_to_serialize = []
	
	# Adds Input port for channel name
	add_input_port(DeckType.Types.STRING, "Channel", "field")
	
	# Adds Trigger for leaving the specified channel
	add_input_port(
		DeckType.Types.ANY,
		"Leave Channel",
		"button",
		Port.UsageType.TRIGGER
	).button_pressed.connect(_receive.bind(1, null))
	


func _receive(to_input_port: int, data: Variant) -> void:
	
	var channel
	
	if to_input_port == 0:
		
		channel = data
	
	if to_input_port == 1 or not channel is String:
		
		channel = await resolve_input_port_value_async(0)
	
	Connections.twitch.leave_channel(channel)
	
