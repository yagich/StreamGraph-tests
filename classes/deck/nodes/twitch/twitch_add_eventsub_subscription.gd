# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode


enum inputs {
	
	event_name,
	sub_data,
	add
	
}

var subscription_data : No_Twitch.EventSub_Subscription

func _init():
	name = "Twitch Add EventSub Subscription"
	node_type = name.to_snake_case()
	description = "Subscribes to a Twitch EventSub Event with the given dictionary 'condition' for the data needed."

	props_to_serialize = []
	
	#Event Name
	add_input_port(
		DeckType.Types.STRING, 
		"Event Name", "field"
	)
	#Subscription Data
	add_input_port(
		DeckType.Types.DICTIONARY, 
		"Subscription Data"
	)
	#Trigger
	add_input_port(
		DeckType.Types.ANY, 
		"Add Subscription", "button",
		Port.UsageType.TRIGGER
	).button_pressed.connect(_receive.bind(inputs.add, null))


func _receive(to_input_port, data: Variant) -> void:
	
	var event
	var sub_data
	
	match to_input_port:
		
		inputs.event_name:
			
			event = str(data)
			sub_data = await resolve_input_port_value_async(1)
			
		inputs.sub_data:
			
			if data == null or not data is Dictionary:
				DeckHolder.logger.log_node("%s: Port %d: Incorrect Subscription Data Connected, please supply a Dictionary with condition and if needed, version.  Last supplied Data was: " + str(data), Logger.LogType.ERROR)
				return
				
			
			sub_data = data
			event = await resolve_input_port_value_async(0)
			
		inputs.add:
			
			event = await resolve_input_port_value_async(0)
			sub_data = await resolve_input_port_value_async(1)
			
		
	
#	Creates an instance of No_Twitch.EventSub_Subscription to store the data with all the given inputs.
	subscription_data = No_Twitch.EventSub_Subscription.new(event, sub_data)
#	Checks if the data has a version field, if so sets it on the EventSub_Subscription
	if sub_data.has("version"):
		
		subscription_data.version = sub_data.version
		
	
#	Calls the connection to add the Subscription
	var req = await Connections.twitch.add_eventsub_subscription(subscription_data)
	req.response_received.connect(eventsub_subscription_response)
	

## Handles checking the [No_Twitch.HTTPResponse] returned by [method No_Twitch.add_eventsub_subscription] to ensure that it succeeded.
func eventsub_subscription_response(data):
	
	match data.code:
		
		202:
			
			var succ_string = name + ": EventSub Subscription Added for " + subscription_data.subscription_type + " successfully"
			DeckHolder.logger.log_node(succ_string, Logger.LogType.INFO)
			Connections.twitch.eventsub_socket.notif_received.connect(Connections._twitch_eventsub_event_received)
			
		
		_:
			
			@warning_ignore("shadowed_global_identifier")
			var error_string = name + ": Error" + data.code + " Received from Twitch when Subscribing to " + subscription_data.sub_type + " with " + str(subscription_data.return_request_dictionary)
			
			DeckHolder.logger.log_node(error_string, Logger.LogType.ERROR)
			
		
	

