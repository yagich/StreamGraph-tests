# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode


func _init():
	name = "Twitch Request User Info"
	node_type = name.to_snake_case()
	description = "Requests a Twitch User's information from the Twitch API."

	props_to_serialize = []
	
#	Adds the User input.
	add_input_port(
		DeckType.Types.STRING, 
		"User ID", 
		"field"
		)
	
#	Adds the Checkbox for using IDs vs Logins and sets it to true.
	add_input_port(
		DeckType.Types.BOOL, 
		"User Input as ID", 
		"checkbox", 
		Port.UsageType.VALUE_REQUEST
		)
	get_input_ports()[1].set_value(true)
	
	get_input_ports()[1].value_updated.connect(switch_user_input_type)
	
	add_output_port(
		DeckType.Types.ANY, 
		"User Info"
		)
	

func switch_user_input_type(value):
	
	var user_port = get_input_ports()[0]
	
	if value:
		
		user_port.label = "User ID"
		
	else:
		
		user_port.label = "User Login"
		
	
	ports_updated.emit()
	


func _receive(port, data) -> void:
	
	if not port == 0:
		
		return
		
	
	data = str(data)
	
	var req_resp = await request_user_info(data, await resolve_input_port_value_async(1))
	send(0, req_resp)
	

func _value_request(_from_port):
	
	var user_input = await resolve_input_port_value_async(0)
	var as_id = await resolve_input_port_value_async(1)
	
	return await request_user_info(user_input, as_id)
	

func request_user_info(user, as_id):
	
	var req_url := "https://api.twitch.tv/helix/users?"
	if as_id:
		
		req_url += "id=" + user
		
	else:
		
		req_url += "login=" + user
		
	
	var resp = Connections.twitch.twitch_request(req_url)
	
	await resp.response_received
	var data = resp.inf_data
	
	return data
	
