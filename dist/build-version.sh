#!/bin/bash

# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

# Main script to build a given version of StreamGraph for Linux and Windows.
# See the files prep-folders.sh and BUILD_TEMPLATE.sh for more information on what happens during the build process.

if [[ $# -ne 1 ]]; then
  echo "ERROR: Version string required."
  exit 2
fi

./prep-folders.sh $1

cd "$1"

./build.sh clean
./build.sh

if [[ $? = 0 ]]; then
  echo "Build completed."
  exit 0
else
  echo "Build failed."
  exit $?
fi
