# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends RPCScope


func _init() -> void:
	name = "deck_holder"
	
	operation_types = {
		#"new_deck": new_deck, # TODO: evaluate later
		"get_deck": {"callable": get_deck, "event_name": ""},
		"send_event": {"callable": send_event, "event_name": ""},
	}
	
	RPCSignalLayer.signals.deck_added.connect(_on_deck_holder_deck_added)
	RPCSignalLayer.signals.deck_closed.connect(_on_deck_holder_deck_closed)


func new_deck(r: RPCRequest) -> void:
	reconnect(
		RPCSignalLayer.signals.deck_added,
		_on_deck_holder_deck_added,
		func():
			var deck_partial := RPCDeckPartial.new()
			deck_partial.id = DeckHolder.add_empty_deck().id
			var resp := create_response(r, deck_partial)
			resp.create_event_counterpart(deck_partial, operation_types)
			response.emit(resp)
	)


func get_deck(r: RPCRequest) -> void:
	if not r.operation.condition.has("id"):
		response.emit(create_error(r, "Invalid Condition"))
		return
	
	var deck := DeckHolder.get_deck(r.operation.condition.id)
	if deck == null:
		response.emit(create_error(r, "Deck doesn't exist"))
		return

	response.emit(create_response(r, deck.to_dict()))


func send_event(r: RPCRequest) -> void:
	if not r.operation.payload.has("event_name"):
		response.emit(create_error(r, "Event name must be present and not empty."))
		return
	
	var event_data: Dictionary
	
	if not r.operation.payload.has("event_data"):
		event_data = {}
	elif not r.operation.payload.event_data is Dictionary:
		#response.emit(create_error(r, "Event data must be an Object"))
		var err := RPCError.new("Event data must be an Object")
		response.emit(create_response(r, {}, err))
		return
	else:
		event_data = r.operation.payload.event_data
	
	DeckHolder.send_event(r.operation.payload.event_name, event_data)
	response.emit(create_generic_success(r))


func _on_deck_holder_deck_added(deck_id: String) -> void:
	event.emit(create_event("new_deck", RPCDeckPartial.new(DeckHolder.get_deck(deck_id))))


func _on_deck_holder_deck_closed(deck_id: String) -> void:
	event.emit(create_event("deck_closed", RPCDeckPartial.new(DeckHolder.get_deck(deck_id))))
