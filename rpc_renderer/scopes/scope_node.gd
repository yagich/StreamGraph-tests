# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends RPCScope


func _init() -> void:
	name = "node"
	
	operation_types = {
		"send" : {"callable" : send, "event_name" : ""},
		"press_button" : {"callable" : press_button, "event_name" : ""},
		"get_input_ports" : {"callable" : get_input_ports, "event_name" : ""},
		"get_output_ports" : {"callable" : get_output_ports, "event_name" : ""},
		"get_virtual_ports" : {"callable" : get_virtual_ports, "event_name" : ""},
		"get_all_ports" : {"callable" : get_all_ports, "event_name" : ""},
		"get_port_value" : {"callable" : get_port_value, "event_name" : ""},
		"set_port_value" : {"callable" : set_port_value, "event_name" : "node_port_value_updated"},
		"get_position" : {"callable" : get_position, "event_name" : ""},
		"set_position" : {"callable" : set_position, "event_name" : "node_position_updated"},
		"rename" : {"callable" : rename, "event_name" : "node_renamed"},
		"get_group_id" : {"callable" : get_group_id, "event_name" : ""},
	}
	
	RPCSignalLayer.signals.node_position_updated.connect(_on_node_position_updated)
	RPCSignalLayer.signals.node_renamed.connect(_on_node_renamed)
	RPCSignalLayer.signals.node_port_value_updated.connect(_on_node_port_value_updated)

# Get Node workflow = Get Deck from ID > Get Node in Deck from ID

# Condition -> Target
# Payload -> Data

func send(req : RPCRequest) -> void:
	
#	Condition
#		deck_id, id, output_port
	
	var node := _get_deck_node(
		req.operation.condition.deck_id, 
		req.operation.condition.id
		)
	
	node.send(req.operation.condition.output_port, req.operation.payload.data)
	
	var resp := create_generic_success(req)
	response.emit(resp)
	

func press_button(req : RPCRequest) -> void:
	
#	Condition
#		deck_id, id, global_port
	
	var node := _get_deck_node(
		req.operation.condition.deck_id, 
		req.operation.condition.id
		)
	
	node.press_button(req.operation.condition.global_port)
	
	var resp := create_generic_success(req)
	response.emit(resp)
	

#region Port Getters
func get_input_ports(req : RPCRequest) -> void:
	
	var node := _get_deck_node(
		req.operation.condition.deck_id, 
		req.operation.condition.id
		)
	
	var ports : Array[RPCPort]
	for all in node.get_input_ports():
		
		ports.append(RPCPort.new(all))
		
	
	var resp = create_response(req, ports)
	response.emit(resp)
	

func get_output_ports(req : RPCRequest) -> void:
	
	var node := _get_deck_node(
		req.operation.condition.deck_id, 
		req.operation.condition.id
		)
	
	var ports : Array[RPCPort] 
	for all in node.get_output_ports():
		
		ports.append(RPCPort.new(all))
		
	
	var resp = create_response(req, ports)
	response.emit(resp)
	

func get_virtual_ports(req : RPCRequest) -> void:
	
	var node := _get_deck_node(
		req.operation.condition.deck_id, 
		req.operation.condition.id
		)
	
	var ports : Array[RPCPort]
	for all in node.get_virtual_ports():
		
		ports.append(RPCPort.new(all))
		
	
	var resp = create_response(req, ports)
	response.emit(resp)
	

func get_all_ports(req : RPCRequest) -> void:
	
	var node := _get_deck_node(
		req.operation.condition.deck_id, 
		req.operation.condition.id
		)
	
	var ports : Array[RPCPort]
	for all in node.get_all_ports():
		
		ports.append(RPCPort.new(all))
		
	
	var resp = create_response(req, ports)
	response.emit(resp)
	
#endregion

#region Port Values
func get_port_value(req : RPCRequest) -> void:
	
	var node := _get_deck_node(
		req.operation.condition.deck_id, 
		req.operation.condition.id
		)
	
	var value = node.get_all_ports()[req.operation.condition.global_port].value
	
	var resp = create_response(req, value)
	response.emit(resp)
	

func set_port_value(req : RPCRequest) -> void:
	
	reconnect(
		RPCSignalLayer.signals.node_port_value_updated, 
		_on_node_port_value_updated, 
		func():
			
			var node := _get_deck_node(
			req.operation.condition.deck_id, 
			req.operation.condition.id
			)
			
			var port = node.get_all_ports()[req.operation.condition.global_port]
			
			# Doesn't show in the Renderer
			port.set_value(req.operation.payload.port_value)
			
			var resp = create_generic_success(req)
			resp.create_event_counterpart({"new_value" : req.operation.payload.port_value}, operation_types)
			response.emit(resp)
	)
	
#endregion

func get_position(req : RPCRequest) -> void:
	
	var node := _get_deck_node(
		req.operation.condition.deck_id, 
		req.operation.condition.id
		)
	
	var resp = create_response(req, node.position)
	response.emit(resp)
	

func set_position(req : RPCRequest) -> void:
	
	reconnect(
		RPCSignalLayer.signals.node_position_updated,
		_on_node_position_updated,
		func():
			var node := _get_deck_node(
				req.operation.condition.deck_id, 
				req.operation.condition.id
				)
			
			node.position = req.operation.payload.position
			
			var resp := create_generic_success(req)
			resp.create_event_counterpart({"new_position" : req.operation.payload.position}, operation_types)
			response.emit(resp)
	)
	

func rename(req : RPCRequest) -> void:
	
	reconnect(
		RPCSignalLayer.signals.node_renamed,
		_on_node_renamed,
		func():
			var node := _get_deck_node(
				req.operation.condition.deck_id, 
				req.operation.condition.id
				)
			
			node.name = req.operation.payload.name
			
			var resp := create_generic_success(req)
			resp.create_event_counterpart({"new_name" :req.operation.payload.name}, operation_types)
			response.emit(resp)
	)
	

# Note, Check if Node IS a group node.
func get_group_id(req : RPCRequest) -> void:
	
	var node := _get_deck_node(
		req.operation.condition.deck_id, 
		req.operation.condition.id
		)
	
	var group_id = node.group_id
	
	var resp := create_response(req, group_id)
	response.emit(resp)
	


func _on_node_position_updated(deck_id: String, node_id: String, position: Dictionary) -> void:
	event.emit(create_event("position_updated", position, {"deck_id": deck_id, "id": node_id}))


func _on_node_renamed(deck_id: String, node_id: String, new_name: String) -> void:
	event.emit(create_event("renamed", {"new_name": new_name}, {"deck_id": deck_id, "id": node_id}))

func _on_node_port_value_updated(deck_id: String, node_id: String, port_idx: int, new_value: Variant) -> void:
	event.emit(create_event(
		"port_value_updated",
		{
			"port": port_idx,
			"new_value": new_value,
		},
		{
			"deck_id": deck_id,
			"id": node_id,
		}
		)
	)

#region Utility Functions
## Utility Functions for getting a Deck from a node_id and a deck_id.
func _get_deck_node(deck_id, node_id) -> DeckNode:
	
	var deck := DeckHolder.get_deck(deck_id)
	var node := deck.get_node(node_id)
	return node
	
#endregion
