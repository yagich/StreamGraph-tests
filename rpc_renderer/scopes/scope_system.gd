# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends RPCScope
class_name RPCScopeSystem


func _init() -> void:
	name = "system"
	operation_types = {
		"identify": {"callable": identify, "event_name": ""},
		"add_subscription": {"callable": add_subscription, "event_name": ""},
		"get_subscriptions": {"callable": get_subscriptions, "event_name": ""},
		"remove_subscriptions": {"callable": remove_subscriptions, "event_name": ""},
	}
	
	RPCSignalLayer.signals.client_connected.connect(_on_client_connected)


func identify(r: RPCRequest) -> void:
	var subscriptions: Dictionary = r.operation.payload.get("subscriptions", {})
	if subscriptions.is_empty():
		subscriptions = {
			"deck": [
				"node_added", "node_removed", "nodes_connected", "nodes_disconnected",
				"nodes_grouped", "variables_updated"
			],
			"deck_holder": [
				"new_deck", "deck_closed"
			]
		}
	r.client.subscriptions = subscriptions
	r.client.identified = true
	
	var resp := create_response(r, {"identified": true})
	response.emit(resp)


func add_subscription(r: RPCRequest) -> void:
	var new_subscriptions: Dictionary = r.operation.payload.subscriptions
	for subscription_scope: String in new_subscriptions:
		var event_names: Array = new_subscriptions[subscription_scope]
		for event: String in event_names:
			if event not in r.client.subscriptions[subscription_scope]:
				(r.client.subscriptions[subscription_scope] as Array).append(event)

	response.emit(create_generic_success(r))


func get_subscriptions(r: RPCRequest) -> void:
	var resp := create_response(r, {"subscriptions": r.client.subscriptions})
	response.emit(resp)


func remove_subscriptions(r: RPCRequest) -> void:
	var to_remove: Dictionary = r.operation.payload.subscriptions
	
	var scopes_to_remove: Array[String]
	for subscription_scope: String in to_remove:
		if subscription_scope not in r.client.subscriptions:
			continue
		var event_names: Array = to_remove[subscription_scope]
		for event: String in event_names:
			(r.client.subscriptions[subscription_scope] as Array).erase(event)
		if (r.client.subscriptions[subscription_scope] as Array).is_empty():
			scopes_to_remove.append(subscription_scope)
	
	for scope in scopes_to_remove:
		r.client.subscriptions.erase(scope)
	
	response.emit(create_generic_success(r))


func _on_client_connected(client: RPCRenderer.Client) -> void:
	var ev := create_event("identify", {})
	ev.to_peer = client.id
	event.emit(ev)
