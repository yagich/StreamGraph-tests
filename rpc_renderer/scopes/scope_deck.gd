# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends RPCScope


func _init() -> void:
	name = "deck"
	
	operation_types = {
		"add_node": {"callable": add_node, "event_name": "node_added"},
		"remove_node": {"callable": remove_node, "event_name": "node_removed"},
		"get_node": {"callable": get_node, "event_name": ""},
		
		"is_valid_connection": {"callable": is_valid_connection, "event_name": ""},
		"connect_nodes": {"callable": connect_nodes, "event_name": "nodes_connected"},
		"disconnect_nodes": {"callable": disconnect_nodes, "event_name": "nodes_disconnected"},
		
		"group_nodes": {"callable": group_nodes, "event_name": "nodes_grouped"},
		
		"set_variable": {"callable": set_variable, "event_name": "variables_updated"},
		"get_variable": {"callable": get_variable, "event_name": ""},
		"get_variable_list": {"callable": get_variable_list, "event_name": ""},
		
		"send_event": {"callable": send_event, "event_name": ""},
		"get_referenced_groups": {"callable": get_referenced_groups, "event_name": ""},
	}

	RPCSignalLayer.signals.deck_node_added.connect(_on_deck_node_added)
	RPCSignalLayer.signals.deck_node_removed.connect(_on_deck_node_removed)
	
	RPCSignalLayer.signals.deck_nodes_connected.connect(_on_deck_nodes_connected)
	RPCSignalLayer.signals.deck_nodes_disconnected.connect(_on_deck_nodes_disconnected)
	RPCSignalLayer.signals.deck_variables_updated.connect(_on_deck_variables_updated)


func add_node(r: RPCRequest) -> void:
	reconnect(
		RPCSignalLayer.signals.deck_node_added,
		_on_deck_node_added,
		func():
			var deck := DeckHolder.get_deck(r.operation.condition.deck_id)
			var node := deck.add_node_type(r.operation.payload.node_type)
			
			var node_partial := RPCNodePartial.new()
			node_partial.deck_id = deck.id
			node_partial.id = node._id
			
			var resp := create_response(r, node_partial)
			resp.create_event_counterpart(node_partial, operation_types)
			response.emit(resp)
	)


func remove_node(r: RPCRequest) -> void:
	reconnect(
		RPCSignalLayer.signals.deck_node_removed,
		_on_deck_node_removed,
		func():
			var deck := DeckHolder.get_deck(r.operation.condition.deck_id)
			var node_id: String = r.operation.payload.node_id
			
			deck.remove_node(node_id, true)
			
			var node_partial := RPCNodePartial.new()
			node_partial.deck_id = deck.id
			node_partial.id = node_id
			
			var resp := create_generic_success(r)
			resp.create_event_counterpart(node_partial, operation_types)
			response.emit(resp)
	)


func get_node(r: RPCRequest) -> void:
	var deck := DeckHolder.get_deck(r.operation.condition.deck_id)
	var node := deck.get_node(r.operation.payload.node_id)
	var node_data := node.to_dict()
	
	var resp := create_response(r, {"node": node_data})
	response.emit(resp)


func is_valid_connection(r: RPCRequest) -> void:
	var deck := DeckHolder.get_deck(r.operation.condition.deck_id)
	var from_node_id: String = r.operation.payload.from_node_id
	var from_output_port := int(r.operation.payload.from_output_port)
	var to_node_id: String = r.operation.payload.to_node_id
	var to_input_port := int(r.operation.payload.to_input_port)
	
	var is_valid = deck.is_valid_connection(from_node_id, to_node_id, from_output_port, to_input_port)
	
	var resp := create_response(r, {"valid": is_valid})
	response.emit(resp)


func connect_nodes(r: RPCRequest) -> void:
	reconnect(
		RPCSignalLayer.signals.deck_nodes_connected,
		_on_deck_nodes_connected,
		func():
			var deck := DeckHolder.get_deck(r.operation.condition.deck_id)
			var from_node_id: String = r.operation.payload.from_node_id
			var from_output_port := int(r.operation.payload.from_output_port)
			var to_node_id: String = r.operation.payload.to_node_id
			var to_input_port := int(r.operation.payload.to_input_port)
			
			var connected := deck.connect_nodes(from_node_id, to_node_id, from_output_port, to_input_port)
			if connected:
				var connection := RPCNodeConnection.new()
				connection.from_node_id = from_node_id
				connection.to_node_id = to_node_id
				connection.from_output_port = from_output_port
				connection.to_input_port = to_input_port
				var resp := create_generic_success(r)
				resp.create_event_counterpart(connection, operation_types)
				response.emit(resp)
			else:
				var err := create_generic_failure(r)
				response.emit(err)
	)


func disconnect_nodes(r: RPCRequest) -> void:
	reconnect(
		RPCSignalLayer.signals.deck_nodes_disconnected,
		_on_deck_nodes_disconnected,
		func():
			var deck := DeckHolder.get_deck(r.operation.condition.deck_id)
			var from_node_id: String = r.operation.payload.from_node_id
			var from_output_port := int(r.operation.payload.from_output_port)
			var to_node_id: String = r.operation.payload.to_node_id
			var to_input_port := int(r.operation.payload.to_input_port)
			
			deck.disconnect_nodes(from_node_id, to_node_id, from_output_port, to_input_port)
			
			var connection := RPCNodeConnection.new()
			connection.from_node_id = from_node_id
			connection.to_node_id = to_node_id
			connection.from_output_port = from_output_port
			connection.to_input_port = to_input_port
			
			var resp := create_generic_success(r)
			resp.create_event_counterpart(connection, operation_types)
			response.emit(resp)
	)


func group_nodes(r: RPCRequest) -> void:
	reconnect(
		RPCSignalLayer.signals.deck_node_removed,
		_on_deck_node_removed,
		func():
			var deck := DeckHolder.get_deck(r.operation.condition.deck_id)
			var node_ids: Array = r.operation.payload.nodes
			var nodes := node_ids.map(
				func(e: String):
					return deck.get_node(e)
			)
			var group := deck.group_nodes(nodes)
			if group == null:
				var err := create_error(r, "Error grouping")
				response.emit(err)
			else:
				var dp := RPCDeckPartial.new(group)
				var resp := create_response(r, dp)
				resp.create_event_counterpart(node_ids, operation_types)
				response.emit(resp)
	)


func set_variable(r: RPCRequest) -> void:
	reconnect(
		RPCSignalLayer.signals.deck_variables_updated,
		_on_deck_variables_updated,
		func():
			var deck := DeckHolder.get_deck(r.operation.condition.deck_id)
			var var_name: String = r.operation.payload.name
			var var_value = r.operation.payload.value
			
			deck.set_variable(var_name, var_value)
			var resp := create_generic_success(r)
			resp.create_event_counterpart({}, operation_types)
			response.emit(resp)
	)


func get_variable(r: RPCRequest) -> void:
	var deck := DeckHolder.get_deck(r.operation.condition.deck_id)
	var var_name: String = r.operation.payload.name
	var value = deck.variable_stack.get(var_name)
	
	var resp := create_response(r, {"name": var_name, "value": value})
	response.emit(resp)


func get_variable_list(r: RPCRequest) -> void:
	var deck := DeckHolder.get_deck(r.operation.condition.deck_id)
	
	var resp := create_response(r, {"variables": deck.variable_stack.keys()})
	response.emit(resp)


func send_event(r: RPCRequest) -> void:
	var deck := DeckHolder.get_deck(r.operation.condition.deck_id)
	var event_name: StringName = r.operation.payload.event_name
	var event_data: Dictionary = r.operation.payload.get("event_data", {})
	
	deck.send_event(event_name, event_data)
	
	var resp := create_generic_success(r)
	response.emit(resp)


func get_referenced_groups(r: RPCRequest) -> void:
	var deck := DeckHolder.get_deck(r.operation.condition.deck_id)
	var groups := deck.get_referenced_groups()
	
	var resp := create_response(r, {"groups": groups})
	response.emit(resp)


func _on_deck_node_added(deck_id: String, node_id: String) -> void:
	event.emit(create_event("node_added", {"node_id": node_id}, {"deck_id": deck_id}))


func _on_deck_node_removed(deck_id: String, node_id: String) -> void:
	event.emit(create_event("node_removed", {"node_id": node_id}, {"deck_id": deck_id}))


func _on_deck_nodes_connected(deck_id: String, from_node_id: String, to_node_id: String, from_output_port: int, to_input_port: int) -> void:
	var connection := RPCNodeConnection.new()
	connection.from_node_id = from_node_id
	connection.to_node_id = to_node_id
	connection.from_output_port = from_output_port
	connection.to_input_port = to_input_port

	var ev := create_event("nodes_connected", connection, {"deck_id": deck_id})
	event.emit(ev)


func _on_deck_nodes_disconnected(deck_id: String, from_node_id: String, to_node_id: String, from_output_port: int, to_input_port: int) -> void:
	var connection := RPCNodeConnection.new()
	connection.from_node_id = from_node_id
	connection.to_node_id = to_node_id
	connection.from_output_port = from_output_port
	connection.to_input_port = to_input_port

	var ev := create_event("nodes_disconnected", connection, {"deck_id": deck_id})
	event.emit(ev)


func _on_deck_variables_updated(deck_id: String) -> void:
	var ev := create_event("variables_updated", {}, {"deck_id": deck_id})
	event.emit(ev)
