# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
class_name RPCFrame
## An RPC frame.
##
## A frame is the second smallest unit of data transmittable over RPC, after base types.

## The name of this frame. When a frame is converted to JSON,
## this will be the key in an object, or if this is the top level frame,
## it will be an object with only one key, which is the value of this property.
var frame_name: String

## The props (key names) that will be serialized to JSON before sending to remote clients.
var _props: Array[StringName]


## Wraps this frame's contents into a dictionary with one key: the [member frame_name]. See [member _props].
## If any prop in [member _props] is also an [RPCFrame], it will be serialized
## using [method to_inner_dict].
func to_dict() -> Dictionary:
	return {frame_name: to_inner_dict()}


## Converts the [i]contents[/i] of this frame to a dictionary.
func to_inner_dict() -> Dictionary:
	var res := {}
	for prop in _props:
		var value = get(prop)
		if value is RPCFrame:
			res[prop] = value.to_dict()
		elif value is Array and not value.is_empty() and value[0] is RPCFrame:
			res[prop] = value.map(
				func(e: RPCFrame):
					return e.to_dict()
			)
		else:
			res[prop] = value
	
	return res
