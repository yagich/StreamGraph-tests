# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends RPCFrame
class_name RPCOperation
## RPC Operation type.
##
## An operation is a part of a request from a remote client. It describes a request based on the type (what to do), the condition (what to do it to) and the payload (what data to do it with).

var type: String
## A condition is an object usually describing which object the operation will apply to.
var condition: Dictionary
## The payload is additional data that may be required to perform an action.
var payload: Dictionary


func _init(p_type: String, p_condition: Dictionary = {}, p_payload: Dictionary = {}) -> void:
	frame_name = "operation"
	_props = [&"type", &"condition", &"payload"]
	
	type = p_type
	condition = p_condition
	payload = p_payload


static func from_dict(d: Dictionary) -> RPCOperation:
	var r := RPCOperation.new(
		d.type,
		d.get("condition", {}),
		d.get("payload", {}),
	)
	
	return r


static func schema() -> Zodot:
	return Z.schema({
		"type": Z.string(),
		"condition": Z.dictionary().nullable(),
		"payload": Z.dictionary().nullable()
	})
