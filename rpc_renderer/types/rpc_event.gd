# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends RPCFrame
class_name RPCEvent
## RPC Event type.
##
## Events are sent when something happens that wasn't initiated by a client.

var id: String = UUID.v4()
var type: String
var scope: String
var data: Variant
var condition: Dictionary

var to_peer: int = 0


func _init(
	p_type: String,
	p_scope: String,
	p_data: Variant,
	p_condition: Dictionary = {},
	) -> void:
		frame_name = "event"
		_props = [
			&"id",
			&"type",
			&"scope",
			&"data",
			&"condition",
		]
		type = p_type
		scope = p_scope
		data = p_data
		condition = p_condition
	
