# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends RPCFrame
class_name RPCNodePartial

var id: String
var deck_id: String


func _init(from: DeckNode = null) -> void:
	frame_name = "node_partial"
	_props = [&"id", &"deck_id"]
	
	if from:
		id = from._id
		deck_id = from._belonging_to.id
