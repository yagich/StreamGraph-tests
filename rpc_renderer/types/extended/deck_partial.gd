# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends RPCFrame
class_name RPCDeckPartial

var id: String
var is_group: bool


func _init(from: Deck = null) -> void:
	frame_name = "deck_partial"
	_props = [&"id"]
	
	if from:
		id = from.id
		is_group = from.is_group
