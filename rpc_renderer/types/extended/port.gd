# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends RPCFrame
class_name RPCPort

var type: DeckType.Types
var label: String
var index: int
var port_type: DeckNode.PortType
var index_of_type: int
var descriptor: String
var usage_type: Port.UsageType

func _init(port : Port):
	frame_name = "port"
	_props = [
		&"type",
		&"label",
		&"descriptor",
		&"port_type",
		&"index_of_type",
		&"index",
		&"usage_type"
	]
	
	type = port.type
	label = port.label
	descriptor = port.descriptor
	
	port_type = port.port_type
	index_of_type = port.index_of_type
	index = port.index
	usage_type = port.usage_type
	
