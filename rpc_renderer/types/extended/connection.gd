# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends RPCFrame
class_name RPCNodeConnection

var from_node_id: String
var from_output_port: int
var to_node_id: String
var to_input_port: int


func _init() -> void:
	frame_name = "node_connection"
	_props = [
		&"from_node_id",
		&"from_output_port",
		&"to_node_id",
		&"to_input_port",
	]
