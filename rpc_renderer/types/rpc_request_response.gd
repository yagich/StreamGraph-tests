# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends RPCFrame
class_name RPCRequestResponse
## RPC Request Response type.
##
## A response to a [RPCRequest]. Initiated by the server as a response. Does not inherently indicate a successful operation.

var id: String = UUID.v4()
## The ID of the request that prompted this response. See [member RPCRequest.id].
var for_request: String
## Broadly, which part of the program this response comes from. See [RPCScope]. The scope will be the same as the [RPCRequest] this is a response for.
var scope: String
## If non-[code]null[/code], the error that this request represents.
var errors: Array[RPCError]
## Additional data associated with the response. Mandatory, but can be empty.
var data: Variant
## See [member RPCRequest.keep].
var kept: Dictionary

## The peer ID this response is intended for. See [member RPCRequest.peer_id].
var peer_id: int

var request: RPCRequest
var event_counterpart: RPCEvent


func _init(p_request: RPCRequest) -> void:
	request = p_request
	frame_name = "request_response"
	_props = [
		&"id",
		&"for_request",
		&"scope",
		&"errors",
		&"data",
		&"kept",
	]


func create_event_counterpart(data: Variant, operation_types: Dictionary) -> void:
	var event_name: String = (operation_types[request.operation.type] as Dictionary).get("event_name", request.operation.type)
	event_counterpart = RPCEvent.new(event_name, scope, data, request.operation.condition)
