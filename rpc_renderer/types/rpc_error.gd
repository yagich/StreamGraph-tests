# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends RPCFrame
class_name RPCError
## Generic RPC error type.

var text: String


func _init(p_text: String) -> void:
	frame_name = "error"
	_props = [&"text"]

	text = p_text
