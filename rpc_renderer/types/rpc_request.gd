# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
class_name RPCRequest
## RPC Request type.
##
## A request is initiated by a remote client that tells the program it wants to do something.

## Provided by the client.
var id: String
## Broadly, which part of the program should handle this request. See [RPCScope].
var scope: String
## The action that the client wants to perform.
var operation: RPCOperation
## Any other data that will be kept and returned back to the client when a response is sent. Optional.
var keep: Dictionary

## Which peer initiated this request.
var peer_id: int

var client: RPCRenderer.Client


static func from_dict(d: Dictionary, p_client: RPCRenderer.Client) -> RPCRequest:
	if not d.has("request"):
		return null
	
	var r := RPCRequest.new()
	r.id = d.request.id
	r.scope = d.request.scope
	r.operation = RPCOperation.from_dict(d.request.operation)
	r.keep = d.request.get("keep", {})
	r.client = p_client
	r.peer_id = p_client.id
	
	return r
