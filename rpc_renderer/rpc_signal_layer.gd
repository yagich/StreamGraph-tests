# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
class_name RPCSignalLayer


static var signals := Signals.new()


#region deck holder
static func _on_deck_added(deck_id: String) -> void:
	signals.deck_added.emit(deck_id)


static func _on_deck_closed(deck_id: String) -> void:
	signals.deck_closed.emit(deck_id)
#endregion


#region deck
static func _on_deck_node_added(node: DeckNode) -> void:
	signals.deck_node_added.emit(node._belonging_to.id, node._id)


static func _on_deck_node_removed(node: DeckNode) -> void:
	signals.deck_node_removed.emit(node._belonging_to.id, node._id)


static func _on_deck_nodes_connected(from_node_id: String, to_node_id: String, from_output_port: int, to_input_port: int, deck_id: String) -> void:
	signals.deck_nodes_connected.emit(deck_id, from_node_id, to_node_id, from_output_port, to_input_port)


static func _on_deck_nodes_disconnected(from_node_id: String, to_node_id: String, from_output_port: int, to_input_port: int, deck_id: String) -> void:
	signals.deck_nodes_disconnected.emit(deck_id, from_node_id, to_node_id, from_output_port, to_input_port)


static func _on_deck_variables_updated(deck_id: String) -> void:
	signals.deck_variables_updated.emit(deck_id)
#endregion


#region node
static func _on_node_position_updated(new_position: Dictionary, node: DeckNode) -> void:
	signals.node_position_updated.emit(node._belonging_to.id, node._id, new_position)


static func _on_node_port_added(port: int, node: DeckNode) -> void:
	signals.node_port_added.emit(node._belonging_to.id, node._id, port, node)


static func _on_node_ports_updated(node: DeckNode) -> void:
	signals.node_ports_updated.emit(node._belonging_to.id, node._id)


static func _on_node_port_value_updated(port_idx: int, new_value: Variant, node: DeckNode) -> void:
	signals.node_port_value_updated.emit(node._belonging_to.id, node._id, port_idx, new_value)


static func _on_node_renamed(new_name: String, node: DeckNode) -> void:
	signals.node_renamed.emit(node._belonging_to.id, node._id, new_name)
#endregion


class Signals:
	#region system
	signal client_connected(client: RPCRenderer.Client)
	#endregion
	
	#region deck holder
	signal deck_added(deck_id: String)
	signal deck_closed(deck_id: String)
	#endregion

	#region deck
	signal deck_node_added(deck_id: String, node_id: String)
	signal deck_node_removed(deck_id: String, node_id: String)
	signal deck_nodes_connected(deck_id: String, from_node_id: String, to_node_id: String, from_output_port: int, to_input_port: int)
	signal deck_nodes_disconnected(deck_id: String, from_node_id: String, to_node_id: String, from_output_port: int, to_input_port: int)
	signal deck_variables_updated(deck_id: String)
	#endregion
	
	#region node
	signal node_position_updated(deck_id: String, node_id: String, new_position: Dictionary)
	signal node_port_added(deck_id: String, node_id: String, port: int)
	signal node_ports_updated(deck_id: String, node_id: String)
	signal node_port_value_updated(deck_id: String, node_id: String, port_idx: int, new_value: Variant)
	signal node_renamed(deck_id: String, node_id: String, new_name: String)
	#endregion
