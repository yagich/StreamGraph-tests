extends WebSocketPeer
class_name Websocket_Client

## Helper Class for handling the freaking polling of a WebSocketPeer

## Emitted when [method WebSocketPeer.get_ready_state()] returns 
## [member WebSocketPeer.STATE_OPEN]
signal socket_open
## Emitted when [method WebSocketPeer.get_ready_state()] returns 
## [member WebSocketPeer.STATE_CLOSED]
signal socket_closed(close_code, close_reason)
## Emitted when [method WebSocketPeer.get_ready_state()] returns 
## [member WebSocketPeer.STATE_CONNECTING]
signal socket_connecting
## Emitted when [method WebSocketPeer.get_ready_state()] returns 
## [member WebSocketPeer.STATE_CLOSING]
signal socket_closing
## Emitted when [method WebSocketPeer.get_available_packets()] returns greater 
## than 0.  Or, when a packet has been received.
signal packet_received(packet_data)

## Works as a wrapper around [method WebSocketPeer.poll] to handle the logic of 
## checking get_ready_state() more simply.
func poll_socket():
	
	poll()
	
	var state = get_ready_state()
	
	match state:
		
		STATE_OPEN:
			
			socket_open.emit(get_connected_host(), get_connected_port())
			
			if get_available_packet_count() > 0:
				
				packet_received.emit(get_packet())
				
			
		
		STATE_CONNECTING:
			
			socket_connecting.emit()
			
		
		STATE_CLOSING:
			
			socket_closing.emit()
			
		
		STATE_CLOSED:
			
			socket_closed.emit(get_close_code(), get_close_reason())
			
		
	

