# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends ConfirmationDialog
class_name RPCSetupDialog

@onready var port_spin_box: SpinBox = %PortSpinBox
@onready var start_server_button: Button = %StartServerButton

var _old_port: int

var is_started: bool = false

signal start_requested(port: int)
signal stop_requested()


func _ready() -> void:
	canceled.connect(
		func():
			port_spin_box.value = float(_old_port)
	)
	
	confirmed.connect(
		func():
			_old_port = int(port_spin_box.value)
	)


func _on_start_server_button_pressed() -> void:
	if not is_started:
		start_server_button.text = "Stop"
		_old_port = int(port_spin_box.value)
		start_requested.emit(int(port_spin_box.value))
		port_spin_box.editable = true
		is_started = true
	else:
		start_server_button.text = "Start"
		stop_requested.emit()
		port_spin_box.editable = true
		is_started = false


func set_port(port: int) -> void:
	port_spin_box.value = port
	_old_port = port


func get_port() -> int:
	return int(port_spin_box.value)
