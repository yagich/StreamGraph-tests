# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends TabContainer
class_name BottomDock

@export var tab_names := ["Console", "Variables"]
@onready var variable_viewer: VariableViewer = %VariableViewer


func _ready() -> void:
	for i in get_child_count():
		set_tab_title(i, tab_names[i])


func rebuild_variable_tree(data: Dictionary) -> void:
	variable_viewer.rebuild_variable_tree(data)
