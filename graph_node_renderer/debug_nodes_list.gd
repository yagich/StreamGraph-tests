# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends VBoxContainer
class_name DebugNodesList


func build(deck: Deck) -> void:
	get_children().map(func(e: Node): e.queue_free())
	for node_id in deck.nodes:
		var b := Button.new()
		var node = deck.get_node(node_id)
		b.text = "%s (%s)" % [node.name, node._id.left(8)]
		b.pressed.connect(
			func():
				DisplayServer.clipboard_set(node_id)
		)
		add_child(b)
