# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends ConfirmationDialog
class_name TwitchSetupDialog

func _ready():
	
	%Authenticate.pressed.connect(authenticate_twitch)
	%CopyURLButton.pressed.connect(copy_auth_link)
	#confirmed.connect(connect_to_chat)
	%Join_Chat.pressed.connect(connect_to_chat)

func authenticate_twitch():
	
	#	Temporary setup for loading credentials
	var loaded_creds = Connections.load_credentials("twitch")
	
	if loaded_creds != null:
		
		Connections.twitch.token = loaded_creds.data.token
		if loaded_creds.data.has("channel"):
			%Default_Chat.text = loaded_creds.data.channel
		Connections.twitch.cache_user_data()
		connect_to_chat()
		return
		
	
	#	Temporary setup for saving tokens.
	Connections.twitch.token_received.connect(func(token):
		
		Connections.save_credentials({"token" : token}, "twitch")
		)
	
	var url = Connections.twitch.authenticate_with_twitch(%Client_ID.text)
	OS.shell_open(url)


func copy_auth_link():
	var url = Connections.twitch.authenticate_with_twitch(%Client_ID.text)
	DisplayServer.clipboard_set(url)


func connect_to_chat():
	
	if not Connections.twitch.get("chat_socket") or Connections.twitch.chat_socket.get_ready_state() != WebSocketPeer.STATE_OPEN:
		Connections.twitch.setup_chat_connection(%Default_Chat.text)
		return
	
	
	Connections.twitch.join_channel(%Default_Chat.text)
