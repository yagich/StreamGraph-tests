# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends VBoxContainer
class_name DebugDecksList

signal item_pressed(deck_id: String, instance_id: String)


func _ready() -> void:
	for i in get_children():
		i.queue_free()

	for deck_id: String in DeckHolder.decks:
		if DeckHolder.decks[deck_id] is Deck:
			var b := Button.new()
			b.text = deck_id
			b.pressed.connect(func():
				item_pressed.emit(deck_id, "")
				)
			var cb := Button.new()
			cb.text = "Copy"
			cb.pressed.connect(
				func():
					DisplayServer.clipboard_set(deck_id)
			)
			
			var hb := HBoxContainer.new()
			hb.add_child(b)
			hb.add_child(cb)
			
			add_child(hb)
		else:
			for instance_id: String in DeckHolder.decks[deck_id]:
				var b := Button.new()
				b.text = "%s::%s" % [deck_id, instance_id]
				b.pressed.connect(func():
					item_pressed.emit(deck_id, instance_id)
					)
				add_child(b)
