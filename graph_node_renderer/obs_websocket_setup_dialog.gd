# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends ConfirmationDialog
class_name OBSWebsocketSetupDialog

@onready var port_spin_box: SpinBox = %PortSpinBox
@onready var password_line_edit: LineEdit = %PasswordLineEdit
@onready var connect_button: Button = %ConnectButton
@onready var obsws_subscriptions_container: PanelContainer = $VBoxContainer/OBSWSSubscriptionsContainer

signal connect_button_pressed(state: ConnectionState)

enum ConnectionState {
	DISCONNECTED,
	CONNECTING,
	CONNECTED,
}
var state: ConnectionState

@warning_ignore("narrowing_conversion")
@onready var _old_port: int = port_spin_box.value
@onready var _old_password: String = password_line_edit.text
@onready var _old_subscriptions: int = obsws_subscriptions_container.get_subscriptions()


func get_port() -> int:
	return int(port_spin_box.value)


func get_password() -> String:
	return password_line_edit.text


func set_button_state(p_state: ConnectionState) -> void:
	connect_button.disabled = p_state == ConnectionState.CONNECTING
	state = p_state

	match p_state:
		ConnectionState.DISCONNECTED:
			connect_button.text = "Connect"
		ConnectionState.CONNECTING:
			connect_button.text = "Connecting..."
		ConnectionState.CONNECTED:
			Connections.save_credentials({
				"port" : get_port(),
				"password" : get_password(),
				"subscriptions": obsws_subscriptions_container.get_subscriptions(),
				}, "OBS")
			connect_button.text = "Disconnect"


func _ready() -> void:
	var loaded_creds = Connections.load_credentials("OBS")
	
	if loaded_creds != null:
		password_line_edit.text = loaded_creds.data.password
		port_spin_box.value = loaded_creds.data.port
		obsws_subscriptions_container.set_subscriptions(loaded_creds.data.subscriptions)
	else:
		obsws_subscriptions_container.set_subscriptions(NoOBSWS.Enums.EventSubscription.ALL)

	connect_button.pressed.connect(
		func():
			
			connect_button_pressed.emit(state)
			
	)

	canceled.connect(
		func():
			print("canceled")
			port_spin_box.value = float(_old_port)
			password_line_edit.text = _old_password
	)

	confirmed.connect(
		func():
			print("confirmed")
			_old_port = int(port_spin_box.value)
			_old_password = password_line_edit.text
	)


func get_subscriptions() -> int:
	return obsws_subscriptions_container.get_subscriptions()
