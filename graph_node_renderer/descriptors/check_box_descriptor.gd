# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DescriptorContainer

@onready var check_box: CheckBox = %CheckBox


func _setup(port: Port, _node: DeckNode) -> void:
	if descriptor.size() > 1:
		check_box.button_pressed = true
	if port.value is bool:
		check_box.button_pressed = port.value
	check_box.text = port.label
	port.value_callback = check_box.is_pressed
	check_box.toggled.connect(port.set_value)
