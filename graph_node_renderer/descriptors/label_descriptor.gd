# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DescriptorContainer

@onready var label: Label = %Label


func _setup(port: Port, _node: DeckNode) -> void:
	label.text = port.label
	if port.port_type == DeckNode.PortType.OUTPUT:
		label.horizontal_alignment = HORIZONTAL_ALIGNMENT_RIGHT
