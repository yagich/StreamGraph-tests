# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DescriptorContainer

@onready var line_edit: LineEdit = %LineEdit


func _setup(port: Port, _node: DeckNode) -> void:
	if port.value:
		line_edit.text = str(port.value)
	line_edit.placeholder_text = port.label
	port.value_callback = line_edit.get_text
	line_edit.text_changed.connect(port.set_value)
