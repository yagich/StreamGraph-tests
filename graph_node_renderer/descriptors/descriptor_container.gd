# meta-description: An empty template with StreamGraph license header.
# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends HBoxContainer
class_name DescriptorContainer

@onready var type_icon: TextureRect = %TypeIcon
var descriptor: Array

const TYPE_ICONS := {
	DeckType.Types.BOOL: preload("res://graph_node_renderer/textures/type_bool.svg"),
	DeckType.Types.NUMERIC: preload("res://graph_node_renderer/textures/type_numeric.svg"),
	DeckType.Types.STRING: preload("res://graph_node_renderer/textures/type_string.svg"),
	DeckType.Types.ARRAY: preload("res://graph_node_renderer/textures/type_array.svg"),
	DeckType.Types.DICTIONARY: preload("res://graph_node_renderer/textures/type_dictionary.svg"),
}


@warning_ignore("unused_parameter")
func set_up_from_port(port: Port, node: DeckNode) -> void:
	descriptor = port.descriptor.split(":")
	_setup(port, node)
	if port.port_type == DeckNode.PortType.VIRTUAL or port.type == DeckType.Types.ANY:
		type_icon.visible = false
		return

	type_icon.modulate = DeckNodeRendererGraphNode.TYPE_COLORS[port.type]
	type_icon.tooltip_text = "Type: %s" % DeckType.type_str(port.type).to_lower()
	type_icon.texture = TYPE_ICONS[port.type]
	if port.port_type == DeckNode.PortType.OUTPUT:
		move_child(type_icon, -1)


@warning_ignore("unused_parameter")
func _setup(port: Port, node: DeckNode) -> void:
	pass
