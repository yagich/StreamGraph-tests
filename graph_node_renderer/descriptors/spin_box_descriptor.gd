# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DescriptorContainer

@onready var spin_box: SpinBox = %SpinBox


func _setup(port: Port, _node: DeckNode) -> void:
	spin_box.tooltip_text = port.label
	if port.value != null:
		spin_box.value = float(port.value)
	if "unbounded" in descriptor:
		spin_box.max_value = 99999
		spin_box.allow_greater = true
		spin_box.allow_lesser = true
		if descriptor.size() > 2:
			spin_box.step = float(descriptor[2])
		else:
			spin_box.step = 1.0
	else:
		spin_box.min_value = float(descriptor[2])
		if descriptor.size() > 2:
			spin_box.max_value = float(descriptor[3])
		if descriptor.size() > 3:
			spin_box.step = float(descriptor[4])
	port.value_callback = spin_box.get_value
	spin_box.value_changed.connect(port.set_value)
