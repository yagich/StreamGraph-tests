# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DescriptorContainer

@onready var code_edit: CodeEdit = %CodeEdit


func _setup(port: Port, _node: DeckNode) -> void:
	if port.value:
		code_edit.text = str(port.value)
	code_edit.placeholder_text = port.label
	port.value_callback = code_edit.get_text
	code_edit.text_changed.connect(port.set_value.bind(code_edit.get_text))
	code_edit.custom_minimum_size = Vector2(200, 100)
	code_edit.size_flags_vertical = SIZE_EXPAND_FILL
