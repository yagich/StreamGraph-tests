# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends VBoxContainer
class_name LoggerRenderer

@onready var output_label: RichTextLabel = %OutputLabel
@onready var copy_button: Button = %CopyButton
@onready var clear_button: Button = %ClearButton

const COLORS := {
	Logger.LogType.INFO: Color.WHITE,
	Logger.LogType.WARN: Color("f4f486"),
	Logger.LogType.ERROR: Color("f47c7c"),
}


func _ready() -> void:
	clear_button.pressed.connect(
		func():
			output_label.clear()
			output_label.text = ""
			output_label.push_context()
	)
	DeckHolder.logger.log_message.connect(_on_logger_log_message)
	output_label.push_context()
	
	# the copy button is disabled for now because it doesnt work
	copy_button.pressed.connect(
		func():
			DisplayServer.clipboard_set(output_label.get_text())
	)


func _on_logger_log_message(text: String, type: Logger.LogType, category: Logger.LogCategory) -> void:
	output_label.pop_context()
	output_label.push_context()
	var category_text: String = Logger.LogCategory.keys()[category].capitalize()
	category_text = "(%s)" % category_text
	output_label.push_bold()
	output_label.add_text("%s: " % category_text)
	output_label.pop()
	output_label.push_color(COLORS[type])
	output_label.add_text(text)
	output_label.newline()
